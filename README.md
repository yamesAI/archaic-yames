![](https://elioway.gitlab.io/dev/archaicyames/elio-archaicyames-logo.png)

> Archaicyames, **the elioWay**

# archaicyames

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [archaicyames Documentation](https://elioway.gitlab.io/dev/archaicyames/)

## Prerequisites

- [archaicyames Prerequisites](https://elioway.gitlab.io/dev/archaicyames/installing.html)

## Installing

- [Installing archaicyames](https://elioway.gitlab.io/dev/archaicyames/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [dev Quickstart](https://elioway.gitlab.io/dev/quickstart.html)
- [archaicyames Quickstart](https://elioway.gitlab.io/dev/archaicyames/quickstart.html)

# Credits

- [archaicyames Credits](https://elioway.gitlab.io/dev/archaicyames/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/dev/archaicyames/apple-touch-icon.png)
